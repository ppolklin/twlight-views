<!DOCTYPE html>
<html class="ch">
<head>
	<?php $this->load->view($vpath.'/partial/head') ?>
</head>

<body ontouchstart="" class="<?=organizationCode?>"> 

<!-- Global Header -->
<header class="masthead" js-inc="/_inc/header.html"></header>

<!-- Page START -->
<main class="mastbody bg-night">
    <div class="top-dots">
        <div class="top-dots-outer">
        <div class="top-dots-inner top-dots-g1"><b class="n1"></b><b class="n3"></b><b class="n5"></b></div>
        <div class="top-dots-inner top-dots-g2"><b class="n2"></b><b class="n4"></b><b class="n6"></b><b class="n7"></b></div>
        </div>
    </div>
    
    <!-- .grids,  grids-xxx,  .griditem 為「列表」通用class
         .newsbrowse,  .newsitem 為「消息列表」客制識別用class-->
    
    <section class="grids  newsbrowse">
        <div class="grids-inner fluid">
            
            <h2 class="grids-maintitle  display-3 h1-mb fw-bolder text-shadow-lg-purple">最新消息</h2> 
            
            <!-- 一頁12則 -->
            <div class="grids-list">
                <ul class="grids-row">
                    <?php if ($data): ?>
                    <?php foreach ($data as $key => $value): ?>
                    <?php  
                    $file = '';
                    if($value['images'])
                    {
                        $file = cdn_url().str_replace('./', '/', $value['images']['listimg']['url'].$value['images']['listimg']['fileName']);
                    }
                    ?>
                    <li class="grids-col">
                        <div class="newsitem  griditem">
                            <a class="griditem-link" href="news_<?=$value['newsID']?>.html">
                                <figure class="griditem-pic">
                                    <img class="lazy" data-src="<?=urlimgSize($file,'600_400')?>" alt="<?=$value['title']?>"><!-- 主圖欄位 小圖 600x400 jpg -->
                                </figure>
                                <!-- 標題欄位 -->
                                <h5 class="griditem-title"><?=$value['title']?></h5>
                            </a>
                            <!-- 日期格式 MM/DD -->
                            <p class="griditem-date  ring-dawn"><span class="text-gradient-dawn"><?=date('m/d', strtotime($value['releaseDate']))?></span></p>
                        </div>
                    </li>
                                         
                    <?php endforeach ?>
                    <?php endif ?>   
                </ul>
            </div><!--/.grids-list-->
            
            <!-- 頁次按鈕 -->
            <?php if ($total_page > 1): ?>
            <nav class="paginav">
                <?php if ($current_page > 1): ?>
                <a class="paginav-item" href="<?=LANGPATH?>/news/index<?=$current_page - 1 > 0 ? '_'.($current_page - 1) : '' ?>.html" aria-label="上一頁"><i class="ico-ft-left-open"></i></a>    
                <?php endif ?>
                <?php for($i=1;$i<=$total_page;$i++):?>
                <a class="paginav-item <?=$current_page == $i ? 'active' : ''?>" href="<?=LANGPATH?>/news/index<?=$i>1?'_'.$i:''?>.html"><?=$i?></a>
                <?php endfor ?>
                <?php if ($total_page > $current_page): ?>
                <a class="paginav-item" href="<?=LANGPATH?>/news/index_<?=$current_page + 1?>.html" aria-label="下一頁"><i class="ico-ft-right-open"></i></a>   
                <?php endif ?>
            </nav>    
            <?php endif ?>
        </div>
    </section>
    
</main>
<!-- Page END -->

<aside class="sponsors" js-inc="/_inc/sponsors.html"></aside>

<footer class="mastfoot" js-inc="/_inc/footer.html"></footer>

<script src="/assets/js/libs/jquery-n-swiper.js"></script>
<script src="/assets/js/main.min.js"></script>

<!-- Optimal to load google font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preload" as="style" 									href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />
<link rel="stylesheet" media="print" onload="this.media='all'"  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />

</body>
</html>