<!DOCTYPE html>
<html class="<?=organizationCode?>">
<head>
    <?php $this->load->view($vpath.'/partial/head') ?>
</head>

<body ontouchstart="" class="<?=organizationCode?>"> 

<!-- Global Header -->
<header class="masthead" js-inc="/_inc/header.html"></header>

<!-- Page START -->
<main class="mastbody bg-night">
    
    <!-- .article,  article-xxx 為「單則內頁」通用class
         .newsview 為「消息內頁」客制識別用class-->
    
    <article class="article  newsview">
        <?php 
        $file = '';
        if($data['images'])
        {
            $file = cdn_url().str_replace('./', '/', $data['images']['listimg']['url'].$data['images']['listimg']['fileName']);
        }
        ?>
        <!-- 主圖欄位(同一張) 1200x800，供背景撐滿，若此項目沒有主圖，請保持此div結構 -->
        <div class="article-bgpic" style="background-image:url(<?=$file?>)"></div>
        
        <div class="article-inner fluid">

            <div class="article-header">
                <!-- 標題欄位-->
                <h2 class="article-title"><?=$data['title']?></h2>
                
                <!-- 資訊欄位-->
                <div class="article-meta">
                    <div class="article-info">
                        <!-- 日期格式 YYYY/MM/DD -->
                        <p class="article-infoitem">
                            <i class="ico-ft-clock big text-blue"></i>
                            <span class="text-gradient-dawn"><?=$data['releaseDate']?></span>
                        </p>
                    </div>
                </div>
            </div>
            
            
            <div class="article-body">
                
                <!-- 主圖欄位(同一張) 1200x800 jpg -->
                <figure class="article-coverpic">
                    <img class="lazy" data-src="<?=$file?>" alt="<?=$data['title']?>">
                </figure>
            
            
                <!-- 簡介欄位，多行文字欄位，支援<br>或<p>斷行，
                     ... 但此欄位要作何用我不清楚，問Claire-->
                <blockquote class="article-intro">
                    <?=nl2br($data['description'])?>
                </blockquote>
                
                <!-- 裝飾用hr -->
                <hr class="bg-magenta">
                    
            
                <!-- 內文欄位，注意 tag 是 <editor> -->
                <editor class="article-content">
                    <?=$data['content']?>
                </editor>
                
            </div><!--/.article-body-->
            
            <div class="article-footer">
                
                <div class="actionbar">
                    <!-- 分享JS已寫，會取 location.href 做分享 -->
                    <div class="sharethis">
                        <span class="sharethis-title  h5 h5-mb fw-bolder">分享到</span>
                        <a class="sharethis-item item--fb" href="javascript:;" arial-label="Facebook"><i class="ico-ft-facebook"></i></a>
                        <a class="sharethis-item item--line" href="javascript:;" arial-label="LINE"><i class="ico-ft-line"></i></a>
                        <a class="sharethis-item item--link" href="javascript:;" arial-label="複製連結 Copy url"><i class="ico-ft-link"></i></a>
                    </div>
                </div>

                <!-- 上下篇 -->
                <nav class="relatednav">
                    <a class="relatednav-prev" href="<?=$prevrow ? LANGPATH.'/news/news_'.$prevrow['newsID']:'javascript:;'?>.html">
                        <?php if ($prevrow): ?>
                        <span class="relatednav-arrow" arial-label="上一篇"></span>
                        <span class="relatednav-title  h4 h4-mb fw-bolder"><?=$prevrow ? $prevrow['title'] : ''?></span>                            
                        <?php endif ?>
                    </a>                        

                    <a class="relatednav-back  fw-bold" href="./">
                        <i class="ico-dots"><b></b></i>返回列表
                    </a>

                    <a class="relatednav-next" href="<?=$nextrow ? LANGPATH.'/news/news_'. $nextrow['newsID'].'.html' : 'javascript:;'?>">
                        <?php if ($nextrow): ?>
                        <span class="relatednav-arrow" arial-label="下一篇"></span>
                        <span class="relatednav-title  h4 h4-mb fw-bolder"><?=$nextrow ? $nextrow['title'] : ''?></span>                            
                        <?php endif ?>
                    </a>                        
                  
                </nav>
            </div><!--/.article-footer-->
        </div><!--/.article-inner-->
        
    </article>
    
</main>
<!-- Page END -->

<aside class="sponsors" js-inc="/_inc/sponsors.html"></aside>

<footer class="mastfoot" js-inc="/_inc/footer.html"></footer>

<script src="/assets/js/libs/jquery-n-swiper.js"></script>
<script src="/assets/js/main.min.js"></script>


<!-- Optimal to load google font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preload" as="style"                                  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />
<link rel="stylesheet" media="print" onload="this.media='all'"  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />

</body>
</html>