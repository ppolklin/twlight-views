<!DOCTYPE html>
<html class="ch">
<head>
	<?php $this->load->view($vpath.'/partial/head') ?>
</head>

<body ontouchstart="" class="<?=organizationCode?>"> 

<!-- Global Header -->
<header class="masthead" js-inc="/_inc/header.html"></header>

<!-- Page START -->
<main class="mastbody bg-night">
    
    <!-- .article,  article-xxx 為「單則內頁」通用class
         .tourview 為「商家內頁」客制識別用class-->
    
    <article class="article  tourview">
        <?php 
        $file = '';
        if($data['images'])
        {
            $file = cdn_url().str_replace('./', '/', $data['images']['listimg']['url'].$data['images']['listimg']['fileName']);
        }
        ?>
        <div class="article-inner fluid">
            
            <div class="article-header">
                <!-- 標題欄位-->
                <h2 class="article-title"><?=$data['title']?></h2>
                
                <!-- 資訊欄位-->
                <div class="article-meta">
                    <div class="article-info">
                        <!-- 商家或表演單位 | 地點-->
                        <p class="article-infoitem">
                            <i class="ico-ft-loc big text-pink"></i>
                            <?=$data['name']?>
                        </p>
                        
                        <!-- 日期 -->
                        <p class="article-infoitem">
                            <i class="ico-ft-clock big text-blue"></i>
                            活動期間：<span class="text-gradient-dawn">即日起 ~ 2022/01/15</span>
                        </p>
                        
                        <!-- 任意欄位範例-->
                        <p class="article-infoitem">
                            <i class="ico-ft-idea big text-light"></i>
                            聯絡電話：037-984989
                            <small class="text-muted">(欄位範例)</small>
                        </p>
                        
                        <!-- 任意欄位範例-->
                        <?php if (!empty($data['website'])): ?>
                        <p class="article-infoitem">
                            <i class="ico-ft-globe big text-light"></i>
                            商家網站：<a class="text-white text-underline" rel="nofollow" href="<?=$data['website']?>" target="_blank">官方網站 <i class="ico-ft-ext"></i></a>
                        </p>                            
                        <?php endif ?>

                    </div>
                </div>
            </div>
            
            
            <div class="article-body">
                
                <!-- 主圖欄位(同一張) 1200x800 jpg -->
                <figure class="article-coverpic">
                    <img class="lazy" data-src="<?=$file?>" alt="<?=$data['title']?>">
                </figure>
            
                <!-- 簡介欄位，多行文字欄位，支援<br>或<p>斷行，
                     ... 但此欄位要作何用我不清楚，問Claire-->
                <blockquote class="article-intro">
                    <?=nl2br($data['description']) ?>
                </blockquote>
                
                <!-- 裝飾用hr -->
                <hr class="bg-magenta">
                
                <!-- 內文欄位，注意 tag 是 <editor> -->
                <editor class="article-content">
                    <?=$data['content'] ?>
                    <!-- 以下這片內容應是由上稿者，透過 HTML編輯器貼入的html碼 -->
                    <p class="h4 fw-bolder"><span class="text-rainbow">優惠方案</span></p>
                    110年2月26日起至110年3月7日<br>
                    滿1000元送100元商品；手工麵線滿2包9折；豆腐乳滿2瓶9折<br>
                    <br>
                    <p class="h4 fw-bolder"><span class="text-rainbow">聯絡資訊</span></p>
                    <i class="ico-ft-right mr-2"></i>電話：037-984989<br>
                    <i class="ico-ft-right mr-2"></i>地址：苗栗縣銅鑼鄉樟樹村四鄰59-1號<br>
                    <i class="ico-ft-right mr-2"></i>網站：<a class="text-magenta text-underline" href="#">官方網站</a>

                </editor>
                
            </div><!--/.article-body-->
            
            <div class="article-footer">
                <!-- 返回列表 -->
                <nav class="relatednav">
                    <a class="relatednav-back  fw-bold" href="./">
                        <i class="ico-dots"><b></b></i>返回列表
                    </a>
                </nav>
            </div><!--/.article-footer-->
        </div><!--/.article-inner-->
        
    </article>
    
</main>

<!-- Page END -->
<aside class="sponsors" js-inc="/_inc/sponsors.html"></aside>

<footer class="mastfoot" js-inc="/_inc/footer.html"></footer>

<script src="/assets/js/libs/jquery-n-swiper.js"></script>
<script src="/assets/js/main.min.js"></script>


<!-- Optimal to load google font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preload" as="style"                                  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />
<link rel="stylesheet" media="print" onload="this.media='all'"  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />

</body>
</html>