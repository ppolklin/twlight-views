<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

<?php if (!empty($metaTitle)): ?>
    <title><?php echo $webset['web_title']['value'] ?>-<?php echo $metaTitle ?></title> 
    <meta property="og:title" content="<?php echo $webset['web_title']['value'] ?>-<?php echo $metaTitle ?>">
<?php else: ?>
    <?php if ( !empty($webTitle)): ?>
    <title><?php echo $webset['web_title']['value'].'-'.$webTitle ?></title> 
    <meta property="og:title" content="<?php echo $webset['web_title']['value'].'-'.$webTitle ?>"> 
    <?php else: ?>   
    <title><?php echo $webset['web_title']['value'] ?></title>
    <meta property="og:title" content="<?php echo $webset['web_title']['value'] ?>">         
    <?php endif ?>
<?php endif ?>

<?php if (!empty($metaDescription)): ?>
<meta name="description" content="<?php echo $metaDescription ?>">
<?php endif ?>

<?php if ( isset($ogimg) && !empty($ogimg)): ?>
<meta property="og:image" content="<?=str_replace(array('34.81.52.152', '34.149.135.201'), 'tw-light.tw', str_replace('http:',  'https:', $ogimg))?>">    
<?php endif ?>

<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/assets/favicon/favicon.png">
<link rel="stylesheet" href="/assets/css/main.css">
