<!DOCTYPE html>
<html class="ch">
<head>
	<?php $this->load->view($vpath.'/partial/head') ?>
</head>

<body ontouchstart="" class="<?=organizationCode?>"> 

<!-- Global Header -->
<header class="masthead" js-inc="/_inc/header.html"></header>

<!-- Page START -->
<main class="mastbody bg-night">
    <div class="top-dots">
        <div class="top-dots-outer">
        <div class="top-dots-inner top-dots-g1"><b class="n1"></b><b class="n3"></b><b class="n5"></b></div>
        <div class="top-dots-inner top-dots-g2"><b class="n2"></b><b class="n4"></b><b class="n6"></b><b class="n7"></b></div>
        </div>
    </div>
    
    <!-- .grids,  grids-xxx,  .griditem 為「列表」通用class
         .eventbrowse,  .eventitem 為「消息列表」客制識別用class-->
    
    <section class="grids  eventbrowse">
        <div class="grids-inner  fluid">
            
            <!-- 衛武營大標 -->
            <h2 class="grids-maintitle  display-3 h1-mb fw-bolder text-shadow-lg-purple"><?=$current_category['title']?></h2>
            <!-- 愛河灣大標 -->
            <!--<h2 class="grids-maintitle  display-3 h1-mb fw-bolder text-shadow-lg-purple">愛河灣活動</h2> -->
            
            <!-- 目前單元 class 加上 `active` -->
            <nav class="grids-catemenu  catemenu  h4 fw-bolder">
                <a class="hover-under" href="<?=LANGPATH?>/news"><span>最新消息</span></a>
                <?php if ($category): ?>
                <?php foreach ($category as $key => $value): ?>
                <a class="hover-under <?=$current_category['code'] == $value['code'] ? 'active' : ''?>" href="<?=LANGPATH?>/event-<?=$value['code']?>"><span><?=$value['title']?></span></a>    
                <?php endforeach ?>                    
                <?php endif ?>
            </nav>
            
            <!-- 一頁12則 -->
            <div class="grids-list">
                <ul class="grids-row">
                <?php if ($data): ?>
                <?php foreach ($data as $key => $value): ?>
                    <?php  
                    $file = '';
                    if($value['images'])
                    {
                        $file = cdn_url().str_replace('./', '/', $value['images']['listimg']['url'].$value['images']['listimg']['fileName']);
                    }
                    ?>                    
                    <li class="grids-col">
                        <div class="eventitem  griditem">
                            <a class="griditem-link" href="event_<?=$value['eventID']?>.html">
                                <figure class="griditem-pic">
                                    <img class="lazy" data-src="<?=urlimgSize($file,'600_400')?>" alt="<?=$value['title']?>"><!-- 主圖欄位 小圖 600x400 jpg -->
                                </figure>
                                <!-- 標題欄位 -->
                                <h5 class="griditem-title"><?=$value['title']?></h5>
                            </a>
                            <div class="griditem-meta">
                                <!-- 商家或表演單位 | 地點-->
                                <p><?=$value['name']?> | <?=$value['loc']?></p>
                                <!-- 日期 -->
                                <p class="small">活動期間：<span class="text-gradient-dawn"><?=$value['startDate']?> ~ <?=$value['endDate']?></span></p>
                            </div>
                            <!-- 按鈕，若沒有則移除 griditem-action 結構 -->
                            <div class="griditem-action">
                                <a class="btn btn-sm btn-outline-blue" href="/booking.html"><span class="d-inline-block lh-sm nowrap">立即預約</span></a>
                            </div>
                        </div>
                    </li>                    
                <?php endforeach ?>
                <?php endif ?>


                </ul>
            </div><!--/.grids-list-->
            
            <!-- 頁次按鈕 -->
            <?php if ($total_page > 1): ?>
            <nav class="paginav">
                <?php if ($current_page > 1): ?>
                <a class="paginav-item" href="<?=LANGPATH?>/event-<?=$current_category['code']?>/index<?=$current_page - 1 > 0 ? '_'.($current_page - 1) : '' ?>.html" aria-label="上一頁"><i class="ico-ft-left-open"></i></a>    
                <?php endif ?>
                <?php for($i=1;$i<=$total_page;$i++):?>
                <a class="paginav-item <?=$current_page == $i ? 'active' : ''?>" href="<?=LANGPATH?>/event-<?=$current_category['code']?>/index<?=$i>1?'_'.$i:''?>.html"><?=$i?></a>
                <?php endfor ?>
                <?php if ($total_page > $current_page): ?>
                <a class="paginav-item" href="<?=LANGPATH?>/event/<?=$current_category['code']?>-index_<?=$current_page + 1?>.html" aria-label="下一頁"><i class="ico-ft-right-open"></i></a>   
                <?php endif ?>
            </nav>    
            <?php endif ?>
            
        </div>
    </section>
    
</main>
<!-- Page END -->

<aside class="sponsors" js-inc="/_inc/sponsors.html"></aside>

<footer class="mastfoot" js-inc="/_inc/footer.html"></footer>

<script src="/assets/js/libs/jquery-n-swiper.js"></script>
<script src="/assets/js/main.min.js"></script>

<!-- Optimal to load google font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preload" as="style"                                  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />
<link rel="stylesheet" media="print" onload="this.media='all'"  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />

</body>
</html>