<!DOCTYPE html>
<html class="ch">
<head>
	<?php $this->load->view($vpath.'/partial/head') ?>
</head>

<body ontouchstart="" class="<?=organizationCode?>"> 

<!-- Global Header -->
<header class="masthead" js-inc="/_inc/header.html"></header>

<!-- Page START -->
<main class="mastbody bg-night">
    
    <!-- .article,  article-xxx 為「單則內頁」通用class
         .eventview 為「活動內頁」客制識別用class-->
    
    <article class="article  eventview">
        <?php 
        $file = '';
        if($data['images'])
        {
            $file = cdn_url(). str_replace('./','/',$data['images']['listimg']['url'].$data['images']['listimg']['fileName']);
        }
        ?>
        <!-- 主圖欄位(同一張) 1200x800，供背景撐滿，若此項目沒有主圖，請保持此div結構 -->
        <div class="article-bgpic" style="background-image:url('<?=$file?>')"></div>
        
        <div class="article-inner fluid">
            
            <div class="article-header">
                <!-- 標題欄位-->
                <h2 class="article-title"><?=$data['title']?></h2>
                
                <!-- 資訊欄位-->
                <div class="article-meta">
                    <div class="article-info">
                        <!-- 商家或表演單位 | 地點-->
                        <p class="article-infoitem">
                            <i class="ico-ft-loc big text-pink"></i>
                            <?=$data['name']?> | <?=$data['loc']?>
                        </p>
                        
                        <!-- 日期 -->
                        <p class="article-infoitem">
                            <i class="ico-ft-clock big text-blue"></i>
                            活動期間：<span class="text-gradient-dawn"><?=$data['startDate']?> ~ <?=$data['endDate']?></span>
                        </p>
                        
                        <!-- 任意欄位範例 -->
                        <!-- <p class="article-infoitem">
                            <i class="ico-ft-idea big text-light"></i>
                            其他欄位：欄位內容值
                            <small class="text-muted">(其他欄位範例)</small>
                        </p> -->
                    </div>
                    
                    <!-- 按鈕 (若沒有就拿掉 div) -->
                    <div class="ml-auto">
                        <a class="btn btn-outline-blue" href="<?=LANGPATH?>/booking.html"><span class="d-inline-block lh-sm p-lg-2 nowrap">立即<br>預約</span></a>
                    </div>
                </div>
            </div>
            
            
            <div class="article-body">
                
                <!-- 主圖欄位(同一張) 1200x800 jpg -->
                <figure class="article-coverpic">
                    <img class="lazy" data-src="<?=$file?>" alt="<?=$data['title']?>">
                </figure>
            
            
                <!-- 簡介欄位，多行文字欄位，支援<br>或<p>斷行，
                     ... 但此欄位要作何用我不清楚，問Claire-->
                <blockquote class="article-intro">
                    <?=nl2br($data['description'])?>
                </blockquote>
                
                
                <!-- 裝飾用hr -->
                <hr class="bg-magenta">
                
                
                <!-- 內文欄位，注意 tag 是 <editor> -->
                <editor class="article-content">
                    
                    
                    <!-- 以下這片內容應是由上稿者，透過 HTML編輯器貼入的html碼 -->
                    <p class="h5 fw-bold  text-center"><i class="ico-ft-clock text-blue mr-2"></i> 演出曲目</p>
                    <?=$data['content']?>
                    <p class="h5 fw-bold  text-center"><i class="ico-ft-clock text-blue mr-2"></i> 活動場次</p>
                    <table class="table text-center">
                        <tbody>
                            <?php if ($schedule): ?>
                            <?php foreach ($schedule as $key => $value): ?>
                            <tr>
                                <td>
                                    <span class="w-50 text-right"><?=$value['date']?></span>&nbsp;&nbsp;&nbsp;
                                    <span class="w-50 h4 fw-bolder text-left"><?=$value['startTime']?> ~ <?=$value['endTime']?> <?=$value['remarks']?></span>
                                </td>
                            </tr>                                
                            <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                    <p class="text-light text-center h6">* 視天候情況加開場次</p>
                    
                    
                </editor>
                
            </div><!--/.article-body-->
            
            <div class="article-footer">
                
                <div class="actionbar">
                    <!-- 按鈕 (若沒有就拿掉 div) -->
                    <div class="ml-auto">
                        <a class="btn btn-lg btn-outline-blue" href="<?=LANGPATH?>/booking.html"><span class="px-lg-5 nowrap">立即預約</span></a>
                    </div>
                </div>
                
                <!-- 返回列表 -->
                <nav class="relatednav">
                    <a class="relatednav-back  fw-bold" href="./">
                        <i class="ico-dots"><b></b></i>返回列表
                    </a>
                </nav>
            </div><!--/.article-footer-->
        </div><!--/.article-inner-->
        
    </article>
    
</main>
<!-- Page END -->

<aside class="sponsors" js-inc="/_inc/sponsors.html"></aside>

<footer class="mastfoot" js-inc="/_inc/footer.html"></footer>

<script src="/assets/js/libs/jquery-n-swiper.js"></script>
<script src="/assets/js/main.min.js"></script>

<!-- Optimal to load google font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preload" as="style"                                  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />
<link rel="stylesheet" media="print" onload="this.media='all'"  href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700&family=Outfit:wght@400;500;700&display=swap" />

</body>
</html>